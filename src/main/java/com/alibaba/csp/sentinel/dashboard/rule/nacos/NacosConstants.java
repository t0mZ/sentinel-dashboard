package com.alibaba.csp.sentinel.dashboard.rule.nacos;

/**
 * @author tom
 */
public final class NacosConstants {

    public static final String FLOW_DATA_ID_POSTFIX = "-flow-rules";
    public static final String DEGRADE_DATA_ID_POSTFIX = "-degrade-rules";
    public static final String PARAM_FLOW_DATA_ID_POSTFIX = "-param-flow-rules";
    public static final String SYS_DATA_ID_POSTFIX = "-sys-rules";
    public static final String AUTH_DATA_ID_POSTFIX = "-auth-rules";
    public static final String CLUSTER_DATA_ID_POSTFIX = "-cluster-rules";

    private NacosConstants(){}
}
