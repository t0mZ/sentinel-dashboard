/*
 * Copyright 1999-2018 Alibaba Group Holding Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.alibaba.csp.sentinel.dashboard.datasource.entity;

import org.influxdb.annotation.Column;
import org.influxdb.annotation.TimeColumn;

import java.time.Instant;
import java.util.Date;

/**
 *  add influx annotation
 * @author tom
 */
//measurement不支持spel
//@Measurement(name="${spring.influx.measurement:resources_metrics}", database = "${spring.influx.db:sentinel_dashboard}")
public class InfluxDbMetricEntity {
//    @Column(name="id")
    private Long id;
    @Column(name="gmtCreate")
    private Long gmtCreate;
//    @Column(name="gmtModified")
    private Date gmtModified;
    @Column(name="app", tag = true)
    private String app;
    /**
     * 监控信息的时间戳
     */
    @TimeColumn
    @Column(name="time")
    private Instant time;
    @Column(name="resource", tag = true)
    private String resource;
    @Column(name="passQps")
    private Long passQps;
    @Column(name="successQps")
    private Long successQps;
    @Column(name="blockQps")
    private Long blockQps;
    @Column(name="exceptionQps")
    private Long exceptionQps;

    /**
     * summary rt of all success exit qps.
     */
    @Column(name="rt")
    private double rt;

    /**
     * 本次聚合的总条数
     */
    @Column(name="count")
    private int count;
//    @Column(name="resourceCode")
    private int resourceCode;

    public static InfluxDbMetricEntity copyOf(InfluxDbMetricEntity oldEntity) {
        InfluxDbMetricEntity entity = new InfluxDbMetricEntity();
        entity.setId(oldEntity.getId());
        entity.setGmtCreate(oldEntity.getGmtCreate());
        entity.setGmtModified(oldEntity.getGmtModified());
        entity.setApp(oldEntity.getApp());
        entity.setTime(oldEntity.getTime());
        entity.setResource(oldEntity.getResource());
        entity.setPassQps(oldEntity.getPassQps());
        entity.setBlockQps(oldEntity.getBlockQps());
        entity.setSuccessQps(oldEntity.getSuccessQps());
        entity.setExceptionQps(oldEntity.getExceptionQps());
        entity.setRt(oldEntity.getRt());
        entity.setCount(oldEntity.getCount());
        entity.setResource(oldEntity.getResource());
        return entity;
    }

    public synchronized void addPassQps(Long passQps) {
        this.passQps += passQps;
    }

    public synchronized void addBlockQps(Long blockQps) {
        this.blockQps += blockQps;
    }

    public synchronized void addExceptionQps(Long exceptionQps) {
        this.exceptionQps += exceptionQps;
    }

    public synchronized void addCount(int count) {
        this.count += count;
    }

    public synchronized void addRtAndSuccessQps(double avgRt, Long successQps) {
        this.rt += avgRt * successQps;
        this.successQps += successQps;
    }

    /**
     * {@link #rt} = {@code avgRt * successQps}
     *
     * @param avgRt      average rt of {@code successQps}
     * @param successQps
     */
    public synchronized void setRtAndSuccessQps(double avgRt, Long successQps) {
        this.rt = avgRt * successQps;
        this.successQps = successQps;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Long gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public String getApp() {
        return app;
    }

    public void setApp(String app) {
        this.app = app;
    }

    public Instant getTime() {
        return time;
    }

    public void setTime(Instant time) {
        this.time = time;
    }

    public String getResource() {
        return resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
        this.resourceCode = resource.hashCode();
    }

    public Long getPassQps() {
        return passQps;
    }

    public void setPassQps(Long passQps) {
        this.passQps = passQps;
    }

    public Long getBlockQps() {
        return blockQps;
    }

    public void setBlockQps(Long blockQps) {
        this.blockQps = blockQps;
    }

    public Long getExceptionQps() {
        return exceptionQps;
    }

    public void setExceptionQps(Long exceptionQps) {
        this.exceptionQps = exceptionQps;
    }

    public double getRt() {
        return rt;
    }

    public void setRt(double rt) {
        this.rt = rt;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getResourceCode() {
        return resourceCode;
    }

    public Long getSuccessQps() {
        return successQps;
    }

    public void setSuccessQps(Long successQps) {
        this.successQps = successQps;
    }

    @Override
    public String toString() {
        return "MetricEntity{" +
            "id=" + id +
            ", gmtCreate=" + gmtCreate +
            ", gmtModified=" + gmtModified +
            ", app='" + app + '\'' +
            ", time=" + time +
            ", resource='" + resource + '\'' +
            ", passQps=" + passQps +
            ", blockQps=" + blockQps +
            ", successQps=" + successQps +
            ", exceptionQps=" + exceptionQps +
            ", rt=" + rt +
            ", count=" + count +
            ", resourceCode=" + resourceCode +
            '}';
    }

    public static InfluxDbMetricEntity convert(MetricEntity oldEntity) {
        InfluxDbMetricEntity entity = new InfluxDbMetricEntity();
        entity.setId(oldEntity.getId());
        entity.setGmtCreate(oldEntity.getGmtCreate().getTime());
        entity.setGmtModified(oldEntity.getGmtModified());
        entity.setApp(oldEntity.getApp());
        entity.setTime(oldEntity.getTimestamp().toInstant());
        entity.setResource(oldEntity.getResource());
        entity.setPassQps(oldEntity.getPassQps());
        entity.setBlockQps(oldEntity.getBlockQps());
        entity.setSuccessQps(oldEntity.getSuccessQps());
        entity.setExceptionQps(oldEntity.getExceptionQps());
        entity.setRt(oldEntity.getRt());
        entity.setCount(oldEntity.getCount());
        entity.setResource(oldEntity.getResource());
        return entity;
    }

    public MetricEntity toMetricEntity() {
        MetricEntity entity = new MetricEntity();
        entity.setId(this.getId());
        entity.setGmtCreate(new Date(this.getGmtCreate()));
        entity.setGmtModified(this.getGmtModified());
        entity.setApp(this.getApp());
        entity.setTimestamp(new Date(this.getTime().toEpochMilli()));
        entity.setResource(this.getResource());
        entity.setPassQps(this.getPassQps());
        entity.setBlockQps(this.getBlockQps());
        entity.setSuccessQps(this.getSuccessQps());
        entity.setExceptionQps(this.getExceptionQps());
        entity.setRt(this.getRt());
        entity.setCount(this.getCount());
        entity.setResource(this.getResource());
        return entity;
    }
}
