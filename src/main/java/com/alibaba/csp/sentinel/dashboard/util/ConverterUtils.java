package com.alibaba.csp.sentinel.dashboard.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;

import java.util.List;

/**
 * @author tom
 */
public class ConverterUtils {

    public static <T> String toJsonString(T t) {
        return JSON.toJSONString(t);
    }

    public static <T> T toJavaObject(String json) {
        return JSON.parseObject(json, new TypeReference<T>(){});
    }

    public static <T> List<T> toListObject(String json, Class<T> clazz) {
        return JSON.parseArray(json, clazz);
    }

    private ConverterUtils() {
    }
}
