Sentinel 控制台
-
#### 说明
    该项目抽取自Sentinel/sentinel-dashboard项目，扩展支持nacos存储的controller和修改了部分js文件
    dashboard面板默认登陆名密码 sentinel/sentinel，默认端口8801，可以通过java -Dargs 覆盖默认值
    
    集成influxdb, 存储metric数据到influxdb. 使用spring-boot2的influxdb实现,更多查看类InfluxDbAutoConfiguration
  实现如下push模式配置规则发布及变动监听
  ![frame](frame.png)
  更多参照: [Sentinel README](README.offical.md)
  
#### 使用
   - 可以打包为jar以后通过java -Darg 形式覆盖默认参数，扩展参数及部分spring参数列表如下(key: defaultValue)
```yaml
# nacos地址，参数形式ip:port， 多个使用英文逗号隔开。
sentinel.dashboard.nacos.server-addr: localhost
# nacos配置data Id的后缀，发布到nacos后对应key为appName-postfix， 如 testNacos-flow-rules
# 下面分别对应流控规则后缀、热点参数规则后缀、降级规则后缀、授权规则后缀、系统规则后缀
nacos.data-id.flow.postfix: -flow-rules
nacos.data-id.param-flow.postfix: -param-flow-rules
nacos.data-id.degrade.postfix: -degrade-rules
nacos.data-id.auth.postfix: -auth-rules
nacos.data-id.sys.postfix: -sys-rules
# 配置influxdb 存储数据库名称和表名,默认取值如下
spring.influx.db: sentinel_dashboard
spring.influx.measurement: resources_metrics


#下面为spring-boot2配置参数,influx数据库地址,默认值如下
spring.influx.url: http://localhost:8086
spring.influx.user: 
spring.influx.password: 
```
  - 客户端
  注册nacos配置监听器及配置sentinel-dashboard地址
  ```java
       public static void main(String[] args) throws NacosException {
           //演示手动指定project.name
           System.setProperty("project.name", APP_NAME);
           //输出到控制台
           System.setProperty(LogBase.LOG_OUTPUT_TYPE, LogBase.LOG_OUTPUT_TYPE_CONSOLE);
   
           //配置dashboard地址
           //可以通过java -Dcsp.sentinel.dashboard.server=127.0.0.1:8801配置，也可以编码配置，多个窦号隔开
           SentinelConfig.setConfig(TransportConfig.CONSOLE_SERVER, "127.0.0.1:8801");
           SpringApplication.run(SentinelSpringBootDemo.class, args);
       }
       
        @PostConstruct
        public void registerNacosDataSources() {
        String flowDataId = APP_NAME + FLOW_DATA_ID_POSTFIX;
        ReadableDataSource<String, List<FlowRule>> flowRowDataSource = new NacosDataSource<List<FlowRule>>(
                remoteAddress, APP_NAME, flowDataId, source -> JSON.parseObject(source, new TypeReference<List<FlowRule>>(){})
        );
        FlowRuleManager.register2Property(flowRowDataSource.getProperty());

        String degradeDataId = APP_NAME + DEGRADE_DATA_ID_POSTFIX;
        ReadableDataSource<String, List<DegradeRule>> degradeDataSource = new NacosDataSource<List<DegradeRule>>(
                remoteAddress, APP_NAME, degradeDataId, source -> JSON.parseObject(source, new TypeReference<List<DegradeRule>>(){})
        );
        DegradeRuleManager.register2Property(degradeDataSource.getProperty());

        String paramDataId = APP_NAME + PARAM_FLOW_DATA_ID_POSTFIX;
        ReadableDataSource<String, List<ParamFlowRule>> paramDataSource = new NacosDataSource<List<ParamFlowRule>>(
                remoteAddress, APP_NAME, paramDataId, source -> {
                    log.info("param flow rules => {}", source);
                    List<SentinelRuleEntity<ParamFlowRule>> rules = JSON.parseObject(source, new TypeReference<List<SentinelRuleEntity<ParamFlowRule>>>(){});
                    return rules.stream().map(r -> r.getRule()).collect(Collectors.toList());
                }
        );
        ParamFlowRuleManager.register2Property(paramDataSource.getProperty());

        String sysDataId = APP_NAME + SYS_DATA_ID_POSTFIX;
        ReadableDataSource<String, List<SystemRule>> sysDataSource = new NacosDataSource<List<SystemRule>>(
                remoteAddress, APP_NAME, sysDataId, source -> JSON.parseObject(source, new TypeReference<List<SystemRule>>(){})
        );
        SystemRuleManager.register2Property(sysDataSource.getProperty());

        String aDataId = APP_NAME + AUTH_DATA_ID_POSTFIX;
        ReadableDataSource<String, List<AuthorityRule>> aDataSource = new NacosDataSource<List<AuthorityRule>>(
                remoteAddress, APP_NAME, aDataId, source -> {
                    log.info("authority rules => {}", source);
                    List<SentinelRuleEntity<AuthorityRule>> rules = JSON.parseObject(source, new TypeReference<List<SentinelRuleEntity<AuthorityRule>>>(){});
                    return rules.stream().map(r -> r.getRule()).collect(Collectors.toList());
                }
        );
        AuthorityRuleManager.register2Property(aDataSource.getProperty());
        }
  ```
 客户端使用demo详细可参照[demo-sentinel-spring-boot](https://gitee.com/t0mZ/demo/tree/master/demo-sentinel-spring-boot)
